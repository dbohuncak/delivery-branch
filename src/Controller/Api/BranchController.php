<?php


namespace App\Controller\Api;


use DeliveryBranch\BranchService;
use DeliveryBranch\Service\Ulozenka;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BranchController
 * @package App\Controller\Api
 */
class BranchController extends ApiController
{
    protected function initApi(Request $request): void
    {
        parent::initApi($request);
        /* todo: pridat moznost vyberu sluzby na zaklade aprametra 'service' */
        $this->service = BranchService::getManager(Ulozenka::class);
    }

    /**
     * @param Request $request  Request class
     * @return JsonResponse
     * @Route ("/api/v1/branch", methods={"GET"},name="api_branch")
     */
    public function indexAction(Request $request): JsonResponse
    {
        $allow = ['internalId','internalName','address','limit', 'page'];
        $this->initApi($request);
        if (!(self::hasOnlyAllowedParams($request, $allow) && $this->service !== null)) {
            return $this->json([]);
        }
        $conditions = [];
        if (null != $address = $request->query->get('address', null)) {
            $address = htmlspecialchars(trim($address),ENT_QUOTES);
            $conditions[] = ['address','=', $address];
        }
        if (null != $id = $request->query->get('internalId', null)) {
            $id = htmlspecialchars(trim($id),ENT_QUOTES);
            $conditions[] = ['internalId','=',(int) $id];
        }

        $content = $this->service->findBy($conditions,[],$this->limit,$this->page);
        return $this->json($content);
    }

    /**
     * @param Request $request  Request class
     * @return JsonResponse
     * @Route ("/api/v1/branch/search",name="api_branch_search")
     */
    public function searchAction(Request $request): JsonResponse
    {
        $this->initApi($request);
        $allow = ['address','limit', 'page'];
        if (!(self::hasOnlyAllowedParams($request, $allow) && $this->service !== null)) {
            return $this->json([]);
        }

        $conditions = [];
        if (null != $address = $request->query->get('address', null)) {
            $address = htmlspecialchars(trim($address),ENT_QUOTES);
            $conditions[] = ['address','like',$address];
        }
        if (empty($conditions)) {
            return $this->json([]);
        }

        $content = $this->service->findBy($conditions,[],$this->limit,$this->page);
        return $this->json($content);
    }

    /**
     * @param int $id           branch ID
     * @param Request $request  Request class
     * @return JsonResponse
     * @Route ("/api/v1/branch/{id}", methods={"GET"}, requirements={"id"="\d+"}, name="api_branch_detail")
     */
    public function idAction(int $id, Request $request): JsonResponse
    {
        $this->initApi($request);
        $allow = [];
        if (!(self::hasOnlyAllowedParams($request, $allow) && $this->service !== null)) {
            return $this->json([]);
        }
        if (null === $content = $this->service->find((int) $id)) {
            return $this->json([]);
        }
        return $this->json([$content]);
    }


}