<?php


namespace App\Controller\Api;


use DeliveryBranch\BranchServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    /**
     * @var int $page current page
     */
    protected $page = 1;

    /**
     * @var int $limit max page limit (0 = all)
     */
    protected $limit = 0;

    /**
     * @var ?BranchServiceInterface
     */
    protected $service = null;

    /**
     * Check if the request has only allowed GET params
     * @param Request $request  current Request
     * @param array $paramKeys  allowed param keys
     * @return bool             if all param keys are OK
     */
    protected static function hasOnlyAllowedParams(Request $request,array $paramKeys): bool
    {
        foreach ($request->query->all() as $key => $value) {
            if (!in_array($key, $paramKeys)) return false;
        }
        return true;
    }

    /**
     * Prepare default variables
     * @param Request $request Current Request
     */
    protected function initApi(Request $request): void
    {
        $this->limit = (int) $request->query->get('limit', 0);
        $this->page = (int) $request->query->get('page', 1);
    }

}