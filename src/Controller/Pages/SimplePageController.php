<?php


namespace App\Controller\Pages;

use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SimplePageController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"})
     */
    public function indexAction(): Response
    {
        return $this->render('demo/content/homepage.html.twig', ['searchLink' => 'tutu']);
    }

    /**
     * @Route("/pobocky", methods={"GET"})
     */
    public function branchAction(): Response
    {
        return $this->render('demo/content/homepage.html.twig', []);
    }

    /**
     * @Route("partals/pobocky/list", methods={"POST"})
     */
    public function branchListAction(): Response
    {
        return $this->render("demo/partials/branch_list.html.twig",[]);
    }

}