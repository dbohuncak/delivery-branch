$(function() {
    console.log( "ready!" );
    // this is the id of the form
    $("#searchForm").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.
        var result;
        var form = $(this);
        var resultBox = $('#searchResults');
        var searchVal = $('input#searchValue').val();
        var loaderBar = $('#loader');
        var url = form.attr('action');

        resultBox.hide().html('');
        loaderBar.show();

        $.ajax({
            type: form.attr('method'),
            url: url,
            dataType: 'json',
            data: form.serialize(), // serializes the form's elements.
            statusCode: {
                200: function() {
                    if (result.length > 0 ) {
                        result.forEach(function(item) {
                            let link = '<a href="'+item.web+'" class="list-group-item list-group-item-action">\n' +
                                '    <div class="d-flex w-100 justify-content-between">\n' +
                                '      <h5 class="mb-1">'+item.address+'</h5>\n' +
                                '    </div>' +
                                '</a>';
                            resultBox.append( link );
                        });
                    } else {
                        let link = '<li class="list-group-item list-group-item-primary">\n' +
                            '    <div class="d-flex w-100 justify-content-between">\n' +
                            '      <h5 class="mb-1">Ľutujeme, ale nenašli sme žiadne výsledky pre výraz "' + searchVal +
                            '"</h5>\n' +
                            '    </div>' +
                            '</li>';
                        resultBox.append( link );
                    }

                },
                404: function() {
                    let link = '<li class="list-group-item list-group-item-danger">\n' +
                        '    <div class="d-flex w-100 justify-content-between">\n' +
                        '      <h5 class="mb-1">Ľutujeme, ale služba je dočasne nedostupná!</h5>\n' +
                        '    </div>' +
                        '</li>';
                    resultBox.append( link );
                },
                500: function() {
                    let link = '<li class="list-group-item list-group-item-danger">\n' +
                        '    <div class="d-flex w-100 justify-content-between">\n' +
                        '      <h5 class="mb-1">Ľutujeme, pri spracovní dat došlo ku chybe!</h5>\n' +
                        '    </div>' +
                        '</li>';
                    resultBox.append( link );
                }
            },
            success: function(data)
            {
                result = data; // show response from the php script.
                loaderBar.hide();
                resultBox.show();
            }
        });
    });
});