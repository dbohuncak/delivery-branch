# Delivery-branch

demo projekt

- symfony 5
- homepage pre testovanie
- lokalny package Delivery-branch (pre demo ucely) cesta: packages\delivery-branch\src 

## Installation & run

```sh
git clone https://gitlab.com/dbohuncak/delivery-branch.git
cd delivery-branch
composer install
php -S 127.0.0.1:8080 -t public
```

## Api
##### api/v1/branch (GET)
---
| nazov | typ | popis  |
| ------ | ------ | ------ |
| limit (optional) | int | maximalny pocet zaznamov (default 0 = nekonecno)|
| page (optional)| int | aktualna strana (default 1)|
| address (optional)| string | vyhladat presne zadany retazec|
| internalName (optional)| string | vyhladat presne zadany retazec|
| internalId (optional)| int | ID zaznamu v sluzbe|

##### api/v1/branch/{id} (GET)
---
| nazov | typ | popis  |
| ------ | ------ | ------ |
| ID | int | ID zaznamu v sluzbe |

##### api/v1/branch/search (GET)
---
| nazov | typ | popis  |
| ------ | ------ | ------ |
| limit (optional) | int | maximalny pocet zaznamov (default 0 = nekonecno)|
| page (optional)| int | aktualna strana (default 1)|
| address (optional)| string | vyhladat retazec v paramtere address |
## License
**Free Software, Hell Yeah!**
