<?php


namespace DeliveryBranch\Service;


use DeliveryBranch\BranchServiceInterface;
use DeliveryBranch\Model\BranchModel;
use DeliveryBranch\Model\BusinessHourModel;
use DeliveryBranch\Model\Coordinates;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;


class Ulozenka implements BranchServiceInterface
{
    /**
     * @var string unique service name
     */
    protected static $serviceName = "ulozenka";

    /**
     * @var string[] array key map
     */
    protected static $ArrayMap = [
        'internalId' => 'id',
        'address' => 'name',
        'internalName' => 'name',
        'web' => 'odkaz',
        'announcements' => 'announcements'
    ];

    /**
     * Convert service array to BranchModel
     * @param $row
     * @return BranchModel
     */
    private static function convertToBranch($row)
    {
        $id = $row['id'] ?? '0';
        $name = $row['name'] ?? '';
        $web = $row['odkaz'] ?? '';
        $location = new Coordinates($row['lat'] ?? 0.0, $row['lng'] ?? 0.0);
        $businessHours = [];
        if (!empty($row['openingHours']) && is_array($row['openingHours'])) {
            foreach ($row['openingHours'] as $hours) {
                $openingHours = new BusinessHourModel();
                $day = $hours['day'] ?? null;
                if (!empty($day)) {
                    $openTime = ($hours['open'] ?? '') . ' - '. ($hours['close'] ?? '');
                    $openingHours->setDayOfWeek($day);
                    $openingHours->setBusinessHour($openTime);
                }
                $businessHours[] = $openingHours;
            }
        }
        $messages = [];
        if (null !== $announcements = $row['announcements'] ?? null &&  is_array($row['announcements'])) {
            $messages = array_column($announcements,'text');
        }

        $model = new BranchModel();
        $model->setLocation($location);
        $model->setBusinessHours($businessHours);
        $model->setInternalId((string) $id);
        $model->setInternalName((string) $name);
        $model->setAddress((string) $name);
        $model->setAnnouncement(implode(' | ', $messages));
        $model->setWeb((string) $web);
        $model->setServiceName(self::$serviceName);
        return $model;
    }

    /**
     * validate row data against conditions
     * @param array $rowData
     * @param array $conditions
     * @return bool
     */
    public static function validRow(array $rowData, array $conditions): bool
    {
        if (empty($conditions) || empty($rowData)) {
            return true;
        }
        try {
            foreach ($conditions as $condition) {
                if (is_array($condition) && count($condition) > 2) {
                    $key = $condition[0] ?? '';
                    if (key_exists($key, self::$ArrayMap) && key_exists(self::$ArrayMap[$key], $rowData)) {
                        $value = strtolower($rowData[self::$ArrayMap[$key]] ?? '');
                        $search = strtolower($condition[2] ?? '');
                        $type = $condition[1];
                        switch ($type) {
                            case '=':
                                $isOk = (strtolower($search) === strtolower($value));
                                break;
                            case 'like':
                                $isOk = str_contains($value, $search);
                                break;
                            default:
                                $isOk = false;
                        }
                        if (!$isOk) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get data from service
     * @return array
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected static function getLocations(): array
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://www.ulozenka.cz/gmap');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        if (!$result) {
            return [];
        }
        curl_close($curl);
        return json_decode($result, true);
    }

    /**
     * @inheritDoc
     */
    public function find(int $id): ?BranchModel
    {
        return $this->findOneBy([['internalId' , '=', $id]]);
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(array $conditions): ?BranchModel
    {
        $result = $this->findBy($conditions,[],0);
        if ((!empty($result)) && $result[0] instanceof BranchModel)  {
            return $result[0];
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function findAll(array $orderBy = []): array
    {
        return $this->findBy([],$orderBy);
    }

    /**
     * @inheritDoc
     */
    public function findBy(array $conditions, array $orderBy = [], int $limit = 0, int $page = 1): array
    {
        $counter = 0;
        $page = ($page < 1) ? 1 : $page;
        $offset = ($page - 1) * $limit;
        $results = [];
        $data = self::getLocations();

        foreach ($data as $row) {
            if (empty($conditions) || self::ValidRow($row, $conditions)) {
                if ($limit > 0) {
                    if ($counter >= $offset) {
                        $results[] = self::convertToBranch($row);
                    }
                } else {
                    $results[] = self::convertToBranch($row);
                }
                $counter++;
                if ($counter == ($offset + $limit)) {
                    return $results;
                }
            }
        }
        return $results;
    }
}
