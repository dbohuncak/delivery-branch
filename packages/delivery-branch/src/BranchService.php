<?php


namespace DeliveryBranch;


use DeliveryBranch\Model\BranchModel;

class BranchService implements BranchServiceInterface
{
    /**
     * @var array list of registered Location managers
     */
    protected static $register = [];

    public static function getManager(string $className): ?BranchServiceInterface
    {
        if (class_exists($className, true)) {
            return new $className();
        }
        return null;
    }

    public function find(int $id): ?BranchModel
    {
        return new BranchModel();
    }

    public function findOneBy(array $conditions): ?BranchModel
    {
        return new BranchModel();
    }

    public function findBy(array $conditions, array $orderBy = [], int $limit = 0, int $page = 1): array
    {
        return [];
    }

    public function findAll(array $orderBy = []): array
    {
        return [];
    }
}