<?php


namespace DeliveryBranch\Model;


class BranchModel
{
    /** @var string */
    private $internalId;

    /** @var string */
    private $internalName;

    /** @var Coordinates */
    private $location;

    /** @var array<BusinessHourModel> */
    private $businessHours;

    /** @var string */
    private $address;

    /** @var string */
    private $web;

    /** @var string */
    private $announcement;

    /** @var string */
    private $serviceName;

    /**
     * @return string
     */
    public function getInternalId(): string
    {
        return $this->internalId;
    }

    /**
     * @param string $internalId
     */
    public function setInternalId(string $internalId): void
    {
        $this->internalId = $internalId;
    }

    /**
     * @return string
     */
    public function getInternalName(): string
    {
        return $this->internalName;
    }

    /**
     * @param string $internalName
     */
    public function setInternalName(string $internalName): void
    {
        $this->internalName = $internalName;
    }

    /**
     * @return Coordinates
     */
    public function getLocation(): Coordinates
    {
        return $this->location;
    }

    /**
     * @param Coordinates $location
     */
    public function setLocation(Coordinates $location): void
    {
        $this->location = $location;
    }

    /**
     * @return array
     */
    public function getBusinessHours(): array
    {
        return $this->businessHours;
    }

    /**
     * @param array $businessHours
     */
    public function setBusinessHours(array $businessHours): void
    {
        $this->businessHours = $businessHours;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getWeb(): string
    {
        return $this->web;
    }

    /**
     * @param string $web
     */
    public function setWeb(string $web): void
    {
        $this->web = $web;
    }

    /**
     * @return string
     */
    public function getAnnouncement(): string
    {
        return $this->announcement;
    }

    /**
     * @param string $announcement
     */
    public function setAnnouncement(string $announcement): void
    {
        $this->announcement = $announcement;
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     */
    public function setServiceName(string $serviceName): void
    {
        $this->serviceName = $serviceName;
    }

}
