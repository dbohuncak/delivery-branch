<?php


namespace DeliveryBranch\Model;

/**
 * Class Coordinates
 * Location coordinates
 * @package DeliveryBranch\Model
 */
class Coordinates
{
    /**
     * @var float $lat gps latitude
     */
    protected $lat;

    /**
     * @var float $lng gps longtitude
     */
    protected $lng;

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng): void
    {
        $this->lng = $lng;
    }

    public function __construct(float $lng = 0.0, float $lat = 0.0 )
    {
        $this->setLng($lng);
        $this->setLat($lat);
    }
}