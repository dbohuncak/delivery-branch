<?php


namespace DeliveryBranch;

use DeliveryBranch\Model\BranchModel;

/**
 * Interface for BranchService classes
 * @package App\BranchService
 */
interface BranchServiceInterface
{
    /**
     * get one location by location ID
     * @param int $id           location ID
     * @return BranchModel|null returns Location entity if successful else null is returned
     */
    public function find(int $id): ?BranchModel;

    /**
     * get one Location entity by search conditions
     * @param array $conditions     array of search conditions
     * @return BranchModel|null     returns Location entity if successful else null is returned
     */
    public function findOneBy(array $conditions): ?BranchModel;

    /***
     * get list of Location entities defined by conditions
     * @param array $conditions list of conditions
     * @param array $orderBy    list of order by
     * @param int $limit        max rows
     * @param int $page         current page
     * @return array<BranchModel>            list of Location entity
     */
    public function findBy(array $conditions, array $orderBy = [], int $limit = 0, int $page = 1): array;

    /**
     * get list of all Location entities
     * @param array $orderBy        list of order by
     * @return array<BranchModel>   list of Location entity
     */
    public function findAll(array $orderBy = []): array;
}
